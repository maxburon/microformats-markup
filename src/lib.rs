use maud::{html, Markup, DOCTYPE, PreEscaped};
use chrono::{Datelike, DateTime};
use microformats_parser::microformats::{Item, Properties, EContent};

pub trait Configuration {
    fn get_root_url(&self) -> String;
    fn get_webmention_url(&self) -> String;
    fn get_css_url(&self) -> Option<String>;
}

// fn create_url(conf: &dyn Configuration, path : &str) -> String {
// format!("{}/{}", conf.get_root_url(), path)
// }

pub fn template(conf: &dyn Configuration, title: Option<&String>, main: Markup) -> Markup {
    html! {
        (DOCTYPE)
            html {
                (head(conf, title))
                    body {
                        (header())
                            main {
                                div id="main-content" {
                                    (main)
                                }
                            }
                    }
            }
    }
}

/// A basic head with a dynamic `page_title`.
fn head(conf: &dyn Configuration, page_title: Option<&String>) -> Markup {
    html! {
        head {
            meta charset="utf-8";
            meta name="viewport" content="width=device-width,initial-scale=1";
            // link rel="icon" href=(create_url(conf, "static/favicon.ico"));
            title { @if let Some(title) = page_title {(title)} @else { "Microformat" } }
            @if let Some(css_url) = conf.get_css_url() {
                link rel="stylesheet" type="text/css" href=(css_url);
            }
            // script src=(create_url(conf, "static/app.js")) defer{}
            link href=(conf.get_webmention_url()) rel="webmention";
        }
    }
}

fn header() -> Markup {
    html! {
        header id="header" {
        }
    }
}

pub fn item_page(item : &Item, conf: &dyn Configuration) -> Markup {
    match &item.properties {
        Properties::EntryProperties{name , url, content, in_reply_to, published,..} => {
            if let Some(names) = name.clone() {
                template(conf, names.first(), entry_markup(name, url, content, in_reply_to, published))
            } else {
                template(conf, None, entry_markup(name, url, content, in_reply_to, published))
            }

        },
        _ => html!{}
    }
}

pub fn item_markup(item : &Item) -> Markup {
    match &item.properties {
        Properties::EntryProperties{name , url, content, in_reply_to, published, ..} => {
            entry_markup(name, url, content, in_reply_to, published)
        },
        _ => html!{}
    }
}

fn entry_markup(entry_names: &Option<Vec<String>>, entry_urls: &Option<Vec<String>>, entry_contents: &Option<Vec<EContent>>, in_reply_to: &Option<Vec<Item>>, entry_published: &Option<Vec<String>>) -> Markup {
    html!{
        article class="h-entry" {

            //   <span class="p-author h-card">
            //     <data class="p-uid" value="tag:twitter.com,2013:pedalpalooza"></data>
            // <data class="p-numeric-id" value="14751786"></data>
            //     <a class="p-name u-url" href="https://twitter.com/pedalpalooza">Pedalpalooza</a>
            // <a class="u-url" href="http://pedalpalooza.org"></a>
            // <a class="u-url" href="http://Shift2bikes.org"></a>
            //     <span class="p-nickname">pedalpalooza</span>
            //     <img class="u-photo" src="https://pbs.twimg.com/profile_images/1399746454231523340/Y-zIlptx.jpg" alt="" />
            //   </span>

            header {
                @if let Some(names) = &entry_names {
                    @for name in names {
                        h3 class="p-name" {(name)};
                    }
                }

                @if let Some(contents) = &entry_contents {
                    @for content in contents {
                        div class="e-content" {
                            @if let Some(html) = &content.html {
                                (PreEscaped(html))
                            }
                            @else if let Some(value) = &content.value {
                                @for line in value.split('\n') {
                                    (line)
                                        br;
                                }
                            } 
                        }
                    }
                }

                @if let Some(in_reply_to_items) = &in_reply_to {
                    @let l = in_reply_to_items.len();
                    p {
                        "In reply to: "
                            @for (index, item) in in_reply_to_items.iter().enumerate() {
                                @if let Some(url) = &item.value {
                                    a href=(url) class="u-in-reply-to" {(url)}
                                }
                                @if index != (l - 1) { ", " } @else {". "}
                            }
                    }
                }

                @if let Some(published) = &entry_published {
                    @if let Some(datestring) = published.first() {
                        @if let Ok(dt) = DateTime::parse_from_rfc3339(datestring) {
                            @if let Some(urls) = &entry_urls {
                                @for url in urls {
                                    time class="dt-published" datetime=(datestring) {
                                        a href=(url) class="u-url"{
                                            "Published on "(dt.day())"/"(dt.month())"/"(dt.year())};
                                    };
                                }
                            } @else {
                                time class="dt-published" datetime=(datestring) {
                                    "Published on "(dt.day())"/"(dt.month())"/"(dt.year()) };
                            }
                        }
                    }
                }

            }
        }
    }
}
